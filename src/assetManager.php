<?php
/**
 * Asset Manager
 *
 * Manage assets with a simple pipeline
 * concatenate and minify css,js
*/
namespace Little\AssetManager ;
/**
 *
 * @uses    Stolz\Assets
 * @see     https://github.com/Stolz/Assets#nonstatic
*/
use Closure;
use Stolz\Assets\Manager ;
use Dflydev\DotAccessData\Data;
/**
* Asset Managment
*
*/
class assetManager {
    protected $config;
    /**
     * Asset Object / Manager
    */
    protected $assets;
    /**
     * Class Constructor
     *
     * Create the instance of the Assets/Manager
     *
     * @param aobject $config  data.config Configuration options
     * @return void
    */
    public function __construct($config,$events,$templateEngine){

        $this->config = new Data($config);
        $this->templateEngine = $templateEngine;

        if( (!is_array($config['assets'])) && ($config['assets'] == null) ){
            $this->config->set('assets', [
                'collections' => array(),
                'autoload' => array(),
                'pipeline' => false,
                //'css_dir' => '',
                //'js_dir' => ''
                ]);
        }
        // Pipeline
        // $this->config->set('assets.public_dir', APP_DIR.TMP_DIR.'assets/');

        $events->addListener('Kore.loaded',function($event,$container){
            $this->assets = new Manager($this->config->get('assets'));
            $container->get('templateEngine')->addGlobal('assets', ['css'=>$this->css(),'js'=>$this->js()]);
        });
    }
  /**
    * add Asset
    *
    * Add an asset or a collection of assets.
    * It automatically detects the asset type (JavaScript, CSS or collection).
    * You may add more than one asset passing an array as argument.
    *
    * @param string|array $assets string / array of assets
    * @return \Little\AssetManager\assetManager
    */
    public function add($assets){
        $this->assets->add($assets);
        return $this;
    }
    /**
     * config()
     *
     * Set up configuration options.
     * All the class properties except 'js' and 'css' are accepted here.
     * Also, an extra option 'autoload' may be passed
     * containing an array of assets and/or collections
     * that will be automatically added on startup.
     *
     * @param array $configuration AssetPipeline configuration options
     * @see https://github.com/Stolz/Assets/blob/master/API.md#config
     *
     **/
    public function config(array $configuration){
        $this->assets->config($configuration);
        return $this;
    }

    public function reset(){
        $this->assets->reset();
        return $this;
    }
  /**
    * css()
    *
    * Build the CSS <link> tags. Accepts an array of $attributes for the HTML tag.
    * You can take control of the tag rendering by providing a closure
    * that will receive an array of assets.
    *
    * @see https://github.com/Stolz/Assets/blob/master/API.md#css-1
    * @param array|Closure $attributes link attributes
    * @return string render css links
    */
    public function css($attributes = null){
        return $this->assets->css($attributes);
    }

    public function js($attributes = null){
        return $this->assets->js($attributes);
    }

    public function registerCollection($collectionName, $assets){
        return $this->assets->registerCollection($collectionName, $assets);
    }

    public function render(){
        return array('css'=>$this->css(),'js'=>$this->js());
    }



}
