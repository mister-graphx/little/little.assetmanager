# Little AssetManager

Lib [assetPipeline](https://github.com/Stolz/Assets) pour Little facilitant la gestion des assets (css,js,cdn), dans les templates/vues.


### Service Configuration

```yaml
parameters:
    plugins: ['templateEngine','assetManager']

    assetManager:
        assets:
            # Relative to the index
            css_dir: '../public/assets/css'
            collections:
            autoload:
             jQuery:
              - 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'
             BootstrapCDN:
              - '//netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'
              - '//netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css'
              - '//netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js'
             css: ['styles.css']

```

## Twig

```twig
{{ assets.css }}
{{ assets.js }}
```

## Collections

https://github.com/Stolz/Assets#sample-collections



```yaml
jQuery-cdn:
    - 'https://code.jquery.com/jquery-3.3.1.slim.min.js'

# https://getbootstrap.com/docs/4.3/getting-started/introduction/
Bootstrap4-cdn:
    - 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'
    - 'jQuery-cdn'
    - 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js'
    - 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js'

# https://milligram.io/
Milligram:
    - '//fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic'
    - '//cdn.rawgit.com/necolas/normalize.css/master/normalize.css'
    - '//cdn.rawgit.com/milligram/milligram/master/dist/milligram.min.css'

```

## Docs

* https://github.com/Stolz/Assets
* https://github.com/Stolz/Assets/blob/master/API.md
* Chemins relatifs ou absolus https://github.com/Stolz/Assets/issues/91
